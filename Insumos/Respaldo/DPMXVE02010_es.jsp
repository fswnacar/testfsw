<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<title>Recepcion de Solicitud</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%@ include file="/atcl_mult_mult_jsp/funciones.jsp"%>
<SCRIPT LANGUAGE="javascript" src="/atcl_es_web_pub/js/utils.js"></SCRIPT>

<% if(!utils.isInternetExplorer()) { %>
<link rel="stylesheet" href="/atcl_es_web_pub/estilos/NacarNS.css" type="text/css">
<% } else { %>
<link rel="stylesheet" href="/atcl_es_web_pub/estilos/NacarIE.css" type="text/css"> 
<% } %>
<script type="text/javascript" src="/dpmx_es_web_pub/js/jquery.js"></script>
<script type="text/javascript" src="/dpmx_es_web_pub/js/jquery.blockUI.js"></script>
<SCRIPT LANGUAGE="javascript" src="/dpmx_es_web_pub/js/utils_dpmx.js"></SCRIPT>
<SCRIPT LANGUAGE="javascript" src="/dpmx_es_web_pub/js/fx_DPMXVE02010.js"></SCRIPT>
<link rel="stylesheet" type="text/css" href="/dpmx_es_web_pub/estilos/Impresiones.css"/>
<!--<link rel="stylesheet" type="text/css" href="/dpmx_es_web_pub/estilos/definicionNacarLigero.css"/>
<link rel="stylesheet" type="text/css" href="/dpmx_es_web_pub/estilos/definicionNacarLigeroChico.css"/>-->

<%

String entorno = System.getProperty("ENTORNO");

String terminacion = "";
if(entorno != null){
	int largo = entorno.length();
	if(largo > 3){ 
		
		terminacion = entorno.substring(entorno.length()-2,entorno.length());
		
	}
}
if(entorno == null){

terminacion = "";
	
}
String estilo = "";
String estilo1 = "";
String acceso = "";
String botones = "";
if(terminacion.equals("cp")|| terminacion.equals("bc")){

	estilo = "../componentesMexico/estilos/estilosNFvR.css";
	botones = "/dpmx_es_web_pub/botones/cp/";

}

if(terminacion.equals("be")){

	estilo = "../componentesMexico/estilos/definicionNacarLigero.css";
	botones = "/dpmx_es_web_pub/botones/be/";

}

if(terminacion.equals("bp")|| terminacion.equals("pm")){

	estilo = "../componentesMexico/patrimonial/estilos/definicionNacarLigero.css";
	botones = "/dpmx_es_web_pub/botones/bp/";
}

if(terminacion.equals("pr")|| terminacion.equals("hn")|| terminacion.equals("bh")){

	estilo = "../prmx_es_web_pub/estilos/definicionNacarLigero.css";
	botones = "/dpmx_es_web_pub/botones/pr/";

}

if(terminacion.equals("bo")||terminacion.equals("fc")){

	estilo = "/dpmx_es_web_pub/estilos/definicionNacarLigeroChico.css";
	estilo1 =  "/dpmx_es_web_pub/estilos/definicionNacarLigero.css";
    acceso = "AC";
	botones = "/dpmx_es_web_pub/botones/";
}
if(terminacion.equals("")){

	estilo = "/dpmx_es_web_pub/estilos/definicionNacarLigeroChico.css";
	estilo1 =  "/dpmx_es_web_pub/estilos/definicionNacarLigero.css";
    acceso = "AC";
	botones = "/dpmx_es_web_pub/botones/";
}
%>
<%
if(acceso.equals("AC")){
%>
<link rel="stylesheet" type="text/css" href="<%=estilo1%>" />
<link rel="stylesheet" type="text/css" href="<%=estilo%>" />
<%											
}else{%>
<link rel="stylesheet" type="text/css" href="<%=estilo%>" />																	
																	
<%	}%>	
<%
	String tipoSolicitud ="";
	String textoSolicitud ="";
	
	tipoSolicitud = utils.getValorContexto("REG_DATOS.NB_TP_SOLICITUD").trim();
	if(tipoSolicitud != ""){
	tipoSolicitud=tipoSolicitud.substring(0,1);
	}
	if(tipoSolicitud.equals("A")){
		textoSolicitud = "Acceso";
	}
	
	if(tipoSolicitud.equals("R")){
		textoSolicitud = "Rectificación";
	}
	
	if(tipoSolicitud.equals("C")){
		textoSolicitud = "Cancelación/Revocación";
	}
	
	if(tipoSolicitud.equals("O")){
		textoSolicitud = "Oposición";
	}	
	%>
		<script> 
			var textoSolicitud = '<%=textoSolicitud%>';
		</script>
	<%
	
%>		
	<!------------------EECC----------------------->
		
	<script type="text/javascript" src="/keon_mult_mult_pub/js/versionCss.js"></script>
	<script type="text/javascript" src="/keon_mult_mult_pub/js/keonCarga.js"></script>
	<script type="text/javascript" src="/keon_mult_mult_pub/js/jquery.js"></script>
	<script type="text/javascript" src="/keon_mult_mult_pub/js/jquery.corner.js"></script>
	<script type="text/javascript" src="/keon_mult_mult_pub/js/jquery.ui.datepicker.js"></script>
	<script type="text/javascript" src="/keon_mult_mult_pub/js/jquery.blockUI.js"></script>
</head>
<body onLoad="controlSesion();inicializa();<% if (utils.getPaginaModoMensaje() != getCteServicioOK()) {%> mostrarMensajes(tipoMensajes,paramMensajes)<%}%>" bgcolor="#FFFFFF">
<customform>
<form name="DPMXVE02010" action="<%=utils.getDestinoFormulario()%>" method="POST" target="" ayudarapida="Formulario Nacar" onSubmit="javascript:if(!isEnviarFormulario()) return false;">
	<customhidden><input name="HD_Mensaje" id="HD_Mensaje" type="hidden" value='<%= utils.getValorContexto("MENSAJE") %>' /></customhidden>
	<customhidden><input name="HD_NB_Derecho" id="HD_NB_Derecho" type="hidden" value='<%= utils.getValorContexto("REG_DATOS.NB_DERECHO") %>' /></customhidden>
	<customhidden><input name="HDtxtSolicitud" id="HDtxtSolicitud" type="hidden" value='<%= utils.getValorContexto("REG_DATOS.TXT_SOLICITUD") %>' /></customhidden>
	<customhidden><input name="HD_TX_COM_CARTA" id="HD_TX_COM_CARTA" type="hidden" value='prueba' /></customhidden>

  <input type="hidden" name="evento" id="evento"><input type="hidden"
	name="flujo" id="flujo" value="<%=utils.getFlujoID()%>"><input
	type="hidden" name="balidaboton" id="balidaboton"
	value="<%=textoSolicitud%>"><input type="hidden" name="ventana"
	id="ventana" value="<%=utils.getVentana()%>"><input
	name="hdDestinoFormulario" id="hdDestinoFormulario" type="hidden"
	value='<%=utils.getDestinoFormulario()%>' />

<div id="pantallaGeneral" style="width:1000px; height:600px;">				
	<div class="DivTituloVentana" id="Cabecera" style="width:970px;">
		<label style="text-align:center" class="tituloVentanaIzq" id="tituloventana1">Solicitud Derecho de&nbsp;<%=textoSolicitud%></label>
	</div>
	<ul class="Pestanas">  
		<li><a class="activo" href="#" title="Solicitud">Solicitud</a></li>
		<li><a class="inactivo" href="#" onClick="lanzaEventos('expediente'); return false;" title="Expediente">Expediente</a></li>
		<li><a class="inactivo" href="#" onClick="lanzaEventos('reporte'); return false;" title="Ver Reporte">Ver Reporte</a></li>
		<li><a class="inactivo" href="#" onClick="lanzaEventos('foliosPrevios'); return false;" title="Folios Previos">Folios Previos</a></li>
		<li><a class="inactivo" href="#" onClick="lanzaEventos('log'); return false;" title="LOG">LOG</a></li>
	</ul> 
	<br/>
	<div id="contenido" style="height:490px; overflow:auto;">
		<div class="DivContenedorPestanasBorde">
			<div class="DivContenedorCaja" style="width:970px;">
				<div class="DivTituloCaja">
					<label class="tituloCajaDer" id="lbl_crit_busqueda"><%=utils.getValorContexto("REG_DATOS.FH_SOLICITUD")%></label>
				</div>
				<div class="Contenido" style="height: auto;">
					<div class="Contenido" style='border-top:1px solid #8c82c0; height: auto;'>
						<table width="98%" border="0">
							<tr>
								<td><img src="/dpmx_es_web_pub/images/comun_logo.gif" border="0" /></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="DivContenedorCaja" style="width:970px;">
				<div class="DivTituloCaja" id="Cabecera2">
					<label class="tituloCajaIzq" id="lbl_crit_busqueda">Sucursal donde se present&oacute; la solicitud</label>
				</div>
				<div class="Contenido">
					<div class="Contenido" style='border-top:1px solid #8c82c0; height: auto;' >
						<table width="98%">
							<tr>
								<td width="2%"></td>
								<td width="15%"><label><b>Nombre:</b></label></td>
								<td width="38%"><label><span id="lblNombreSucursal"
									class="contexto" estatica="false"><%=utils.getValorContexto("REG_DATOS.NB_CR")%></span></label>
								</td>
								<td width="15%"><label><b>Folio:</b></label></td>
								<td width="28%"><label><span id="lblFolio" class="contexto" estatica="false"><%=utils.getValorContexto("REG_DATOS.CD_SOLICITUD")%></span></label>
								</td>
							</tr>
							<tr>
								<td width="2%"></td>
								<td width="15%"><label><b>Ejecutivo:</b></label></td>
								<td width="38%"><label>
									<customlabel>
										<span id="lblNombreEjecutivo" class="contexto" estatica="false"><%=utils.getValorContexto("REG_DATOS.P_NB_EJECUTIVO")%></span></customlabel>
											&nbsp;<span id="lblApPatEjecutivo" class="contexto" estatica="false"><%=utils.getValorContexto("REG_DATOS.P_NB_PAT_EJECUTIVO")%></span>
										&nbsp;
										<span id="lblApMatEjecutivo" class="contexto" estatica="false"><%=utils.getValorContexto("REG_DATOS.P_NB_MAT_EJECUTIVO")%></span></label>
								</td>
								<td width="15%"><label><b>Suc. (CR):</b></label></td>
								<td width="28%"><label><span id="lblSucursal"
									class="contexto" estatica="false"><%=utils.getValorContexto("REG_DATOS.CD_CR")%></span></label>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="DivContenedorCaja" style="width:970px;">
				<div class="DivTituloCaja" id="Cabecera3">
					<label class="tituloCajaIzq" id="lbl_crit_busqueda">Datos Personales del Solicitante (Titular)</label>
				</div>
				<div class="Contenido">
					<div class="Contenido" style='border-top:1px solid #8c82c0; height: auto;' >
						<table width="98%">
							<tr>
								<td width="2%"></td>
								<td width="15%"><label><b>No. Cliente:</b></label></td>
								<td width="38%"><label><span id="lblNumCliente" class="contexto" estatica="false"><%=utils.getValorContexto("REG_DATOS.NU_CLIENTE")%></span></label>
								</td>
								<td width="15%"><label><b>R.F.C.:</b></label></td>
								<td width="28%"><label><span id="lblRfc" class="contexto" estatica="false"><%=utils.getValorContexto("REG_DATOS.NB_RFC")%></span></label>
								</td>
							</tr>
							<tr>
								<td width="2%"></td>
								<td width="15%"><label><b>Nombre(s):</b></label></td>
								<td width="38%"><label><span id="lblNomCliente" class="contexto" estatica="false"><%=utils.getValorContexto("REG_DATOS.NB_CLIENTE")%></span></label>
								</td>
								<td width="15%"><label><b>A. Paterno:</b></label></td>
								<td width="28%"><label><span id="lblApePaterno" class="contexto" estatica="false"><%=utils.getValorContexto("REG_DATOS.NB_APPATERNO")%></label>
								</td>
							</tr>
							<tr>
								<td width="2%"></td>
								<td width="15%"><label><b>A. Materno:</b></label></td>
								<td width="38%"><label><span id="lblApeMaterno" class="contexto" estatica="false"><%=utils.getValorContexto("REG_DATOS.NB_APMATERNO")%></span></label>
								</td>
								<td width="15%"><label><b>CURP:</b></label></td>
								<td width="28%"><label><span id="lblCurp" class="contexto" estatica="false"><%=utils.getValorContexto("REG_DATOS.NB_CURP")%></span></label>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="DivContenedorCaja" style="width:970px;">
				<div class="DivTituloCaja" id="Cabecera3">
					<label class="tituloCajaIzq" id="lbl_crit_busqueda">Domicilio</label>
				</div>
				<div class="Contenido">
					<div class="Contenido" style='border-top:1px solid #8c82c0; height: auto;' >
						<table width="98%">
							<tr>
								<td width="2%"></td>
								<td width="15%"><label><b>Calle:</b></label></td>
								<td width="38%"><label><span id="lblCalle" class="contexto" estatica="false"><%=utils.getValorContexto("REG_DATOS.NB_CALLE")%></span></label>
								</td>
								<td width="15%"><label><b>N&uacute;mero Ext.:</b></label></td>
								<td width="28%"><label><span id="lblNumExt" class="contexto" estatica="false"><%=utils.getValorContexto("REG_DATOS.NU_EXTERIOR")%></span></label>
								</td>
							</tr>
							<tr>
								<td width="2%"></td>
								<td width="15%"><label><b>N&uacute;mero Int.:</b></label></td>
								<td width="38%"><label><span id="lblNumInt" class="contexto" estatica="false"><%=utils.getValorContexto("REG_DATOS.NU_INTERIOR")%></span></label>
								</td>
								<td width="15%"><label><b>C&oacute;digo Postal:</b></label></td>
								<td width="28%"><label><span id="lblCodPostal" class="contexto" estatica="false"><%=utils.getValorContexto("REG_DATOS.NU_COD_POSTAL")%></span></label>
								</td>
							</tr>
							<tr>
								<td width="2%"></td>
								<td width="15%"><label><b>Colonia:</b></label></td>
								<td width="38%"><label><span id="lblColonia" class="contexto" estatica="false"><%=utils.getValorContexto("REG_DATOS.NB_COLONIA")%></span></label>
								</td>
								<td width="15%"><label><b>Entre Calle:</b></label></td>
								<td width="28%"><label><span id="lblEntreCalle1" class="contexto" estatica="false"><%=utils.getValorContexto("REG_DATOS.NB_ENTRE_CALLE1")%></span></label>
								</td>
							</tr>
							<tr>
								<td width="2%"></td>
								<td width="15%"><label><b>Y Calle:</b></label></td>
								<td width="38%"><label><span id="lblEntreCalle2" class="contexto" estatica="false"><%=utils.getValorContexto("REG_DATOS.NB_ENTRE_CALLE2")%></span></label>
								</td>
								<td width="15%"><label><b>Entidad Federativa:</b></label></td>
								<td width="28%"><label><span id="lblEntidadFed" class="contexto" estatica="false"><%=utils.getValorContexto("REG_DATOS.NB_ENTIDAD_FED")%></span></label>
								</td>
							</tr>
							<tr>
								<td width="2%"></td>
								<td width="15%"><label><b>Delegaci&oacute;n o Municipio</b></label></td>
								<td width="38%"><label><span id="lblDelegacion" class="contexto" estatica="false"><%=utils.getValorContexto("REG_DATOS.NB_DELEGACION")%></span></label>
								</td>
								<td width="15%"><label><b></b></label></td>
								<td width="28%"><label></label></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="DivContenedorCaja" style="width:970px;">
				<div class="DivTituloCaja" id="Cabecera4">
					<label class="tituloCajaIzq" id="lbl_crit_busqueda">Datos de Identificaci&oacute;n</label>
				</div>
				<div class="Contenido">
					<div class="Contenido" style='border-top:1px solid #8c82c0; height: auto;' >
						<table width="98%">
							<tr>
								<td width="2%"></td>
								<td width="15%"><label><b>N&uacute;mero Documento:</b></label></td>
								<td width="38%"><label><span id="lblIdentificacion" class="contexto" estatica="false"><%=utils.getValorContexto("REG_DATOS.TP_IDOFICIAL")%></span></label>
								</td>
								<td width="15%"><label><b>Folio:</b></label></td>
								<td width="28%"><label><span id="lblDocumento" class="contexto" estatica="false"><%=utils.getValorContexto("REG_DATOS.NU_DOCUMENTO")%></span></label></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="DivContenedorCaja" style="width:970px;">
				<div class="DivTituloCaja" id="Cabecera5">
					<label class="tituloCajaIzq" id="lbl_crit_busqueda">Medio Alterno de Notificaciones</label>
				</div>
				<div class="Contenido">
					<div class="Contenido" style='border-top:1px solid #8c82c0; height: auto;' >
						<table width="98%">
							<tr>
								<td width="2%"></td>
								<td width="15%"><label><b>Correo Electr&oacute;nico:</b></label></td>
								<td width="38%"><label><span id="lblCorreo" class="contexto" estatica="false"><%=utils.getValorContexto("REG_DATOS.NB_CORREO")%></span></label>
								</td>
								<td width="15%"><label><b>Tel&eacute;fono:</b></label></td>
								<td width="28%"><label><span id="lblTelefono" class="contexto" estatica="false"><%=utils.getValorContexto("REG_DATOS.NU_TELEFONO")%></span></label></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="DivContenedorCaja" style="width:970px;">
				<div class="DivTituloCaja" id="Cabecera6">
					<label class="tituloCajaIzq" id="lbl_crit_busqueda">Campo a Ejercer Derecho</label>
				</div>
				<div class="Contenido">
					<div class="Contenido" style='border-top:1px solid #8c82c0; height: auto;' >
						<table width="98%">
							<tr>
								<td width="2%"></td>
								<td width="94%" align="left"><label><span id="lblDerecho" class="contexto" estatica="false"><%=utils.getValorContexto("REG_DATOS.NB_DERECHO")%></span></label></td>
								<td width="2%"></td>
							</tr>
							<tr>	
								<td width="2%"></td>
								<td width="2%"><label><b>Descripci&oacute;n Clara y Precisa de la Solicitud:</b></label></td>
							</tr>
							<tr>
								<td width="2%"></td>
								<td width="94%" align="left"><label><span id="lblDescripcion" class="contexto" estatica="false"><%=utils.getValorContexto("REG_DATOS.TX_DESCRIPCION")%></span></label></td>
								<td width="2%"></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="DivContenedorCaja" style="width:970px;">
				<div class="DivTituloCaja" id="Cabecera7">
					<label class="tituloCajaIzq" id="lbl_crit_busqueda">Comentario</label>
				</div>
				<div class="Contenido">
					<div class="Contenido" style='border-top:1px solid #8c82c0; height: auto;' >
						<table width="98%">
							<tr>
								<td>
									<textarea class="contexto" id="txComCarta" style='text-align: justify; width:960px; height:75px; resize:none;' onKeyUp="habBtnGuardar();">
									prueba
									</textarea>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="Contenido" style="width:97%">
				<table width="98%">
					<tr>
						<td width="10%"></td>
						<td width="22%">
							<input name="btnRegresa" id="btnRegresa" type="image" style="vertical-align: bottom cursor:pointer"   border="0" onclick="lanzaEventos('regresa'); return false;"  disabled="disabled" src="<%=botones%>regresar_1.gif" />
						</td>
						<!--<td width="22%">
							<input name="btnExpediente" id="btnExpediente" type="image" style="vertical-align: bottom cursor:pointer"   border="0" onclick="lanzaEventos('expediente'); return false;"  disabled="disabled" src="<%=botones%>Expediente_1.gif" />
						</td>-->
						<td width="22%">
							<input name="btnInprimir" id="btnInprimir" type="image" style="vertical-align: bottom cursor:pointer"   border="0" onclick="lanzaEventos('inprimir'); return false;"  disabled="disabled" src="<%=botones%>imprimir_1.gif" />
						</td>
						<!--<td width="22%">
							<input name="btnReporte" id="btnReporte" type="image" style="vertical-align: bottom cursor:pointer"   border="0" onclick="lanzaEventos('reporte'); return false;"  disabled="disabled" src="<%=botones%>Ver_Reporte_1.gif" />
						</td>-->
						<td width="22%">
							<input name="btnGuardar" id="btnGuardar" type="image" style="vertical-align: bottom cursor:pointer"   border="0" onclick="lanzaEventos('guardar'); return false;"  disabled="disabled" src="<%=botones%>guardar_1.gif" />
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>
</form>
</customform>
</body>
</html>