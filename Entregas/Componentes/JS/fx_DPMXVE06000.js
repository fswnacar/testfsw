var front;

function inicializa()
{
	mensajeError();
	mensajeErrorImax();
	validaBotonAnadir(document.getElementById("TFtipodocumento"));
	
	if(document.getElementById("HDBanderaRelanzar").value == "S"){
	
	lanzaEventos('relanzar');
	
	}
}

function mensajeErrorImax()
{
	var error = document.getElementById("HDerror").value;
	var respu = document.getElementById("HDrespuesta").value;	
	if(error != ""){
		error = error.replace(/<respuesta>/g," ");
		error = error.replace(/<\/respuesta>/g," ");
		error = error.replace(/&lt;/g," ");
		error = error.replace(/&gt;/g," ");
		error = error.replace(/<error>/g," ");
		error = error.replace(/<\/error>/g," ");
		//keonMensaje('error','Error',error,'cerrar','cerrarMsj()');
		alert(error);
	}
	
	if(respu != ""){
		respu = respu.replace(/<respuesta>/g," ");
		respu = respu.replace(/<\/respuesta>/g," ");
		respu = respu.replace(/&lt;/g," ");
		respu = respu.replace(/&gt;/g," ");
		respu = respu.replace(/<error>/g," ");
		respu = respu.replace(/<\/error>/g," ");
		//keonMensaje('error','Error',respu,'cerrar','cerrarMsj()');
		alert(respu);
	}
}


function validaBotonAnadir(obj)
{
	var descri = document.getElementById("HDnb_documentos");
	var codigo = document.getElementById("HDcd_documentos");
	
	if(obj.selectedIndex > 0){		
		document.getElementById("anadir0").style.display = "none";
		document.getElementById("anadir1").style.display = "block";
	}else{
		document.getElementById("anadir0").style.display = "block";
		document.getElementById("anadir1").style.display = "none";
	}
	
	codigo.value = obj.value;	
	descri.value = obj.options[obj.selectedIndex].text;
}

function validaSeleccionGrilla()
{
	var opcion = 0;	
	for(var i=0; i<largo; i++){
		var objeto = document.getElementById("radio"+i);
		if(objeto.checked){
			opcion = 1;
		}
	}
	return opcion;
}

function cambiaEstilo(obj)
{
	if(obj.value!= ""){
		obj.className = "CapturaNormal";		
	}else{
		obj.className = "CapturaRequerida";
	}
}

function mensajeError()
{
	var codigo = document.getElementById("HDcd_error");
	var descripcion = document.getElementById("HDnb_error");
	
	if(codigo.value != "")	
	{
		alert(descripcion.value);
		//keonMensaje('error','Error',descripcion.value,'cerrar','cerrarMsj()');
	}
}

function lanzaEventos(opcion)
{
	switch(opcion){
		case 'regresar':
			lanzaEventoDPMX('0X2801003');
		break;
		case 'anadir':
			document.getElementById("HDoperacion").value = "ws:guardaArchivos";
			lanzaEventoDPMX('0X02801000');
		break;
		case 'Eliminar':
			document.getElementById("HDoperacion").value = "ws:ejecutaProcesos";
			document.getElementById("HD_cadena_entrada").value = "IDPROCESO|NUMCLIENTE|FOLIOARCO|CLVEDOCTO|USUARIO|TIPODOCTO|VERSION";
			
			document.getElementById("HD_cadena_valores").value = "ELIMINADOCTO"+"|"+document.getElementById("TFnumclie").value+"|"+document.getElementById("TFfolio").value+"|"+document.getElementById("HDcd_documentos").value+"|"+document.getElementById("HDperfil").value;
			
			lanzaEventoDPMX('0X280100B');
		break;
		case 'documentos':
			lanzaDocumento();
			// lanzaEventoDPMX('0X280100B');
		break;
		case 'relanzar':
			lanzaEventoDPMX('0X2801002');
			// lanzaEventoDPMX('0X280100B');
		break;
		
	}
}


/***************************************************************************/

function lanzaDocumento()
{
	
	//1ro conformar URL
	//var urlEstatica = "http://150.100.131.207:9081/fimxarco/visor-externo?"; // parte estatica
	var urlEstatica = "http://"+ip+":"+puerto+visor;
	

	//2do variables
	var aplicacion = "ARCO";
	var version = "1";
	var clavedocto = document.getElementById("HDcd_documentos").value;

	var urlDinamica = "aplicacion="+aplicacion+"&cliente="+numCliente+"&folio="+folCliente+"&clavedocto="+clavedocto+"&version=1"; // parte dinamica	
	var urlFinal = urlEstatica+urlDinamica;

	//lanzo la modal.
	//alert(urlFinal);
	
	window.open(urlFinal,"popup","width=700,height=520");	
}

//Deja los campos en hiddens para luego poder conformar la trama

function seleccionRadio(indice, famdoc, tpodoc, foldoc, fecexp, fecven, fecalta, estdig, cvedoc, fecvend)
{
	var hd_famdoc = document.getElementById("HDfamdoc");
	var hd_tpodoc = document.getElementById("HDtpodoc");
	var hd_foldoc = document.getElementById("HDfoldoc");
	var hd_fecexp = document.getElementById("HDfecexp");
	var hd_fecven = document.getElementById("HDfecven");
	var hd_fecalta = document.getElementById("HDfecalta");
	var hd_estdig = document.getElementById("HDestdig");
	var hd_fecvend = document.getElementById("HDfecvend");
	
	hd_famdoc.value = famdoc;
	hd_tpodoc.value = tpodoc;
	hd_foldoc.value = foldoc;
	hd_fecexp.value = fecexp;
	hd_fecven.value = fecven;
	hd_fecalta.value = fecalta;
	hd_estdig.value = estdig;
	hd_fecvend.value = fecvend;
	
	document.getElementById("HDcd_documentos").value = cvedoc;
	
	if( (tpodoc == "CARTA RESPUESTA PDP" ||  tpodoc == "MAIL RESPUESTA PDP" || tpodoc == "ACUSE CARTA RESPUES") && (cvedoc == "220" || cvedoc == "221" || cvedoc == "222") ){
		document.getElementById("Eliminar1").style.display = "block";
		document.getElementById("Eliminar0").style.display = "none";
	}else{
		document.getElementById("Eliminar1").style.display = "none";
		document.getElementById("Eliminar0").style.display = "block";
	}
	
}

var numCliente = "";
var nomCliente = "";
var folCliente = "";

function verDocumento(estatus)
{
	if(estatus == "N"){
		document.getElementById("verdocumento1").style.display = "none";
		document.getElementById("verdocumento0").style.display = "block";
	}else{
		document.getElementById("verdocumento1").style.display = "block";
		document.getElementById("verdocumento0").style.display = "none";
	}
	
	numCliente = document.getElementById("TFnumclie").value;
	nomCliente = document.getElementById("TFnomclie").value;
	folCliente = document.getElementById("TFfolio").value;
}

/***************************************************************************/

function activaUpload()
{
	document.getElementById("uploadArchivo").click();
}

function validaAnadir(obj)
{
	if(obj.value!=""){
		var n=obj.value.split('\\');
		var ruta = obj.value;
		var nombre = n[n.length-1];
		
		// document.getElementById("HD_Ruta_File").value = ruta;
		document.getElementById("HD_Nombre_File").value = nombre;
		
		//Confecciona el string previo a enviar a IMAX
		
		var numcli = document.getElementById("TFnumclie").value;
		var nomcli = document.getElementById("TFnomclie").value;
		var folarc = document.getElementById("TFfolio").value;
		var clvdto = document.getElementById("HDcd_documentos").value;
		var nomdct = document.getElementById("HDnb_documentos").value;
		var sucurs = document.getElementById("HDsucursal").value;
		var usuari = document.getElementById("HDperfil").value;
		
		var cadena_entrada = "NUMCLIENTE|NOMCLIENTE|FOLIOARCO|CLVEDOCTO|NOMBREDOCTO|SUCURSAL|USUARIO|EXTENSION|TIPODOCTO|PRIMERA|ULTIMA";
		var cadena_valores = numcli+"|"+nomcli.trim()+"|"+folarc+"|"+clvdto+"|"+nomdct+"|"+sucurs+"|"+usuari;		
	
		document.getElementById("HD_cadena_entrada").value = cadena_entrada;
		document.getElementById("HD_cadena_valores").value = cadena_valores;
		//Para las pruebas daremos un valor a HDtipodocto
		// document.getElementById("HDtipodocto").value = "O";
		lanzaEventos('anadir');
	}
}

if(!String.prototype.trim){
  String.prototype.trim = function(){
    return this.replace(/^\s+|\s+$/g, ''); 
  } 
}

function MoverScroll(sLeft,sTop,caller){	
	document.getElementById(sLeft).scrollLeft=document.getElementById(caller).scrollLeft;
	document.getElementById(sTop).scrollTop=document.getElementById(caller).scrollTop;
}

function setPestana(ev){
	lanzaEventoDPMX(ev);
}