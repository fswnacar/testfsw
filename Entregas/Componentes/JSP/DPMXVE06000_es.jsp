<%@ page import ="java.io.FileInputStream" %>
<%@ page import ="java.io.FileNotFoundException" %>
<%@ page import ="java.io.IOException" %>
<%@ page import ="java.net.InetAddress" %>
<%@ page import ="java.net.MalformedURLException" %>
<%@ page import ="java.net.URL" %>
<%@ page import ="java.util.Properties" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page import = "atae.apli.contexto.*" %>
<%@ page import="java.util.Date, java.text.SimpleDateFormat,java.text.DecimalFormat" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<title>EXPEDIENTE SOLICITUD</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<%@include file="/atcl_mult_mult_jsp/funciones.jsp"%>
	<SCRIPT LANGUAGE="javascript" src="/atcl_es_web_pub/js/utils.js"></SCRIPT>
	<script type="text/javascript" src="/dpmx_es_web_pub/js/jquery.js"></script>
	<script type="text/javascript" src="/dpmx_es_web_pub/js/jquery.blockUI.js"></script>
	<SCRIPT LANGUAGE="javascript" src="/dpmx_es_web_pub/js/utils_dpmx.js"></SCRIPT>
	<SCRIPT LANGUAGE="javascript">var nom_jsp="DPMXVE06000";</SCRIPT>
	<%@include file="/dpmx_es_web_jsp/estilosDPMX.jsp"%>
<link rel="stylesheet" type="text/css" href="/dpmx_es_web_pub/estilos/definicionNacarLigero.css"/>
<link rel="stylesheet" type="text/css" href="/dpmx_es_web_pub/estilos/definicionNacarLigeroChico.css"/>
	
	<!--Mejora Sistemas LFPDP ODT2-->
	<link rel="stylesheet" type="text/css" href="/keon_mult_mult_pub/estilos/NacarFF1024v02.css"/>
	
	<script>var front="<%=entTerm%>";</script>
		
	<SCRIPT LANGUAGE="javascript" src="/dpmx_es_web_pub/js/fx_DPMXVE06000.js"></SCRIPT>
	
	<%!	public String obtenerElementoNoNulo(atae.apli.contexto.AtaeSvCompositeDatoRegistro registro, String elemento){
			if(registro.getElemento(elemento) != null && registro.getElemento(elemento).getValor() != null){
				return registro.getElemento(elemento).getValor().trim();
			}
			return "";
		}
	%>
	
	<%!
		public String formateaFecha(String fecha, String separador1, String separador2)
		{
			String newDate = ""; 			
			if(fecha.length() > 0 && fecha != null){
				String[] aux = fecha.split("-");
				newDate = aux[2]+separador2+aux[1]+separador2+aux[0];
			}else{
				newDate = "";
			}
			
			return newDate;
		}
	%>
	
	<%
	
	String ip = "";
	String puerto = "";
	String visor = "";
	
	
	try{
		Properties properties;
		String entorno = System.getProperty("ENTORNO");
	
		String propertiesDirectory = null;
		FileInputStream fileinputstream;
		
		if(entorno != null){
		propertiesDirectory = "/" + entorno + "/dpmx/online/multipais/multicanal/cfg/srvnegocio.dpmx.properties";
		}
		
		if(entorno == null){
			propertiesDirectory = "D:/nacar/RAD/nacar/WebContent/dpmx/online/multipais/multicanal/cfg/srvnegocio.dpmx.properties";
		}

		properties = new Properties();
		fileinputstream = new FileInputStream(propertiesDirectory);
		properties.load(fileinputstream);
		ip  = properties.getProperty("WS.HOST");
		puerto  = properties.getProperty("WS.PORT");
		visor  = properties.getProperty("WS.VISOR");
		
	}catch(Exception ex){
		
	}	
		
%>

	<script language='javascript' type="text/javascript">	
	
	var ip ='<%=ip%>';
	var puerto ='<%=puerto%>';
	var visor ='<%=visor%>';
	
	</script>

	<!------------------EECC----------------------->
		
	<script type="text/javascript" src="/keon_mult_mult_pub/js/versionCss.js"></script>
	<script type="text/javascript" src="/keon_mult_mult_pub/js/keonCarga.js"></script>
	<script type="text/javascript" src="/keon_mult_mult_pub/js/jquery.js"></script>
	<script type="text/javascript" src="/keon_mult_mult_pub/js/jquery.corner.js"></script>
	<script type="text/javascript" src="/keon_mult_mult_pub/js/jquery.ui.datepicker.js"></script>
	<script type="text/javascript" src="/keon_mult_mult_pub/js/jquery.blockUI.js"></script>	
	</head>
	<body onload="controlSesion(); inicializa();" bgcolor="#FFFFFF" style="overflow-x:hidden">
		<customform> 
			<form name="DPMXVE06000" action="<%=utils.getDestinoFormulario()%>" method="POST" target="" ayudarapida="Formulario Nacar" onSubmit="javascript:if(!isEnviarFormulario()) return false;" enctype="multipart/form-data">
				<input type="hidden" name="evento">
				<input type="hidden" name="flujo" value="<%=utils.getFlujoID()%>">
				<input type="hidden" name="ventana" value="<%=utils.getVentana()%>">
				
				<!--HIDDEN PARA SALIDA DEL SP-->		
				<customhidden><input name="HDnb_documentos" id="HDnb_documentos" type="hidden" value='<%= utils.getValorContexto("REG_VENTANA.NB_DOCUMENTOS") %>' /></customhidden>
				<customhidden><input name="HDcd_documentos" id="HDcd_documentos" type="hidden" value='<%= utils.getValorContexto("REG_VENTANA.CD_DOCUMENTOS") %>' /></customhidden>
				<customhidden><input name="HDnb_error" id="HDnb_error" type="hidden" value='<%= utils.getValorContexto("REG_VENTANA.NB_ERROR") %>' /></customhidden>
				<customhidden><input name="HDcd_error" id="HDcd_error" type="hidden" value='<%= utils.getValorContexto("REG_VENTANA.CD_ERROR") %>' /></customhidden>
				
				<!--HIDDEN PARA EL ESTRING DE ADJUNTAR-->
				<customhidden><input name="HD_Ruta_File" id="HD_Ruta_File" type="hidden" value='<%= utils.getValorContexto("REG_VENTANA.RUTA") %>' /></customhidden>
				<customhidden><input name="HD_Nombre_File" id="HD_Nombre_File" type="hidden" value='<%= utils.getValorContexto("REG_VENTANA.NOMBRE") %>' /></customhidden>

				<customhidden><input name="HDfamdoc" id="HDfamdoc" type="hidden" value='<%= utils.getValorContexto("REG_VENTANA.FAMDOC") %>' /></customhidden>
				<customhidden><input name="HDtpodoc" id="HDtpodoc" type="hidden" value='<%= utils.getValorContexto("REG_VENTANA.TPODOC") %>' /></customhidden>
				<customhidden><input name="HDfoldoc" id="HDfoldoc" type="hidden" value='<%= utils.getValorContexto("REG_VENTANA.FOLDOC") %>' /></customhidden>
				<customhidden><input name="HDfecexp" id="HDfecexp" type="hidden" value='<%= utils.getValorContexto("REG_VENTANA.FECEXP") %>' /></customhidden>
				<customhidden><input name="HDfecven" id="HDfecven" type="hidden" value='<%= utils.getValorContexto("REG_VENTANA.FECVEN") %>' /></customhidden>
				<customhidden><input name="HDfecalta" id="HDfecalta" type="hidden" value='<%= utils.getValorContexto("REG_VENTANA.FECALTA") %>' /></customhidden>
				<customhidden><input name="HDestdig" id="HDestdig" type="hidden" value='<%= utils.getValorContexto("REG_VENTANA.ESTDIG") %>' /></customhidden>
				<customhidden><input name="HDfecvend" id="HDfecvend" type="hidden" value='<%= utils.getValorContexto("REG_VENTANA.FECVEND") %>' /></customhidden>


				<customhidden><input name="HDperfil" id="HDperfil" type="hidden" value='<%= utils.getValorContexto("REG_VENTANA.USUARIO") %>' /></customhidden>
				<customhidden><input name="HDsucursal" id="HDsucursal" type="hidden" value='<%= utils.getValorContexto("REG_VENTANA.SUCURSAL") %>' /></customhidden>

				<customhidden><input name="HDtipodocto" id="HDtipodocto" type="hidden" value='<%= utils.getValorContexto("REG_VENTANA.TIPODOCTO") %>' /></customhidden>
				<customhidden><input name="HDoperacion" id="HDoperacion" type="hidden" value='<%= utils.getValorContexto("REG_VENTANA.OPERACION") %>' /></customhidden>


				<customhidden><input name="HD_cadena_entrada" id="HD_cadena_entrada" type="hidden" value='<%= utils.getValorContexto("CADENA_ENTRADA") %>' /></customhidden>
				<customhidden><input name="HD_cadena_valores" id="HD_cadena_valores" type="hidden" value='<%= utils.getValorContexto("CADENA_VALORES") %>' /></customhidden>
				

				<customhidden><input id="HDerror" name="HDerror" type="hidden" value='<%= utils.getValorContexto("REG_VENTANA.ERROR") %>' /></customhidden>
				<customhidden><input id="HDrespuesta" name="HDrespuesta" type="hidden" value='<%= utils.getValorContexto("REG_VENTANA.RESPUESTA") %>' /></customhidden>
				<customhidden><input id="HDBanderaRelanzar" name="HDBanderaRelanzar" type="hidden" value='<%= utils.getValorContexto("RELANZAR") %>' /></customhidden>

				<%
					String textoSolicitud = utils.getValorContexto("REG_VENTANA.TXT_SOLICITUD");
					String texto = "";
					
					if(!textoSolicitud.equals("")){
						texto = textoSolicitud;
					}
				%>
				
<div id="pantallaGeneral" style="width:1000px; height:580px; overflow:hidden;">
  <div id="contenido" style="height:525px; overflow:auto;">
    <div class="DivTituloVentana" id="Cabecera" style="width:970px;">
      <label style="text-align:center" class="tituloVentanaIzq" id="tituloventana1">EXPEDIENTE SOLICITUD DE ACCESO</label>
    </div>
	<ul class="Pestanas">  
		<li><a class="inactivo" href="#" onClick="setPestana('0X280102A');" title="Solicitud">Solicitud</a></li>
		<li><a class="activo"   href="#" title="Expediente">Expediente</a></li>
		<li><a class="inactivo" href="#" onClick="setPestana('0X280102B');" title="Ver Reporte">Ver Reporte</a></li>
		<li><a class="inactivo" href="#" onClick="setPestana('0X280102C');" title="Folios Previos">Folios Previos</a></li>
		<li><a class="inactivo" href="#" onClick="setPestana('0X280102D');" title="LOG">LOG</a></li>
	</ul>
    <div class="DivTituloCaja" style="width:970px;">
		<label class="tituloCajaDer" id="lbl_crit_busqueda">
										<%
												
												String fecha = utils.getValorContexto("REG_VENTANA.FECHA2");
												if(fecha!= null && !fecha.equals("") ){
													String mayuscula=fecha.charAt(0)+"";
													mayuscula=mayuscula.toUpperCase();
													fecha=fecha.replaceFirst(fecha.charAt(0)+"", mayuscula);
												}else{
													fecha="";
												}
											%><%=fecha%>
		</label>
    </div>
	
	
	
	<div class="DivContenedorPestanasBorde">
		<div class="DivContenedorCaja" style="width:970px;">
		<div class="DivTituloCaja">
			<label class="tituloCajaIzq" id="lbl_crit_busqueda">Expediente Solicitud de <%=textoSolicitud%></label>
		</div>
		<div class="Contenido" style="height: auto;">
			<div class="Contenido" style='border-top:1px solid #8c82c0; height: auto;'>
			<table width="98%" border="0">
				<tr>
				<td width="2%"></td>
				<td width="11%"><!--<label style="font-color:red"><b>Folio Contingencia</b></label> -->
								<script>
								bandera ='<%=utils.getValorContexto("REG_VENTANA.CD_SOL_CONTINGENCIA")%>';
								ver=''
								if (bandera=='' || bandera==null){							
								ver='hidden';
								}else{
								document.write('<label style="font-color:red"><b>Folio Contingencia</b></label>');
								ver='visible';
								}
								</script>			  
				
				</td>
				<td width="11%">
				<script>
								
								document.write('<div style=visibility:'+ver+'>');
								</script>
				
				<inputfieldtext> <span id="TFfocon_ER" class='<%=utils.escribeEstiloLabel("REG_VENTANA.CD_SOL_CONTINGENCIA")%>'></span>
						<input
											type="text" 
											name="TFfocon" 
											id="TFfocon"
											value='<%=utils.getValorContexto("REG_VENTANA.CD_SOL_CONTINGENCIA")%>'
											requerido="false" 
											size="15" 
											maxlength="15" 
											class="Deshabilitado"
											sentido="N" 
										/>
						<%=getIndicadorRequeridoDerecha(false)%> </inputfieldtext>
						
						</div>
				</td>
				<td width="11%"></td>
				</tr>
				<tr>
				<td width="2%"></td>
				<td width="11%"><label style="font-color:red"><b>Folio</b></label></td>
				<td width="11%"><inputfieldtext> <span id="TFfolio_ER" class='<%=utils.escribeEstiloLabel("REG_VENTANA.FOLIO")%>'></span>
						<input
											type="text" 
											name="TFfolio" 
											id="TFfolio"
											value='<%=utils.getValorContexto("REG_VENTANA.FOLIO")%>'
											requerido="false" 
											size="15" 
											maxlength="15" 
											class="Deshabilitado"
											sentido="N" 
										/>
						<%=getIndicadorRequeridoDerecha(false)%> </inputfieldtext>
				</td>
				<td width="11%"></td>
				</tr>
				<tr>
				<td width="2%"></td>
				<td width="11%"><label style="font-color:red"><b>Cliente</b></label></td>
				<td width="11%"><inputfieldtext> <span id="TFnumclie_ER" class='<%=utils.escribeEstiloLabel("REG_VENTANA.NUMCLIE")%>'></span>
						<input 
											type="text" 
											name="TFnumclie" 
											id="TFnumclie"
											value='<%=utils.getValorContexto("REG_VENTANA.NUMCLIE")%>'
											requerido="false" 
											size="8" 
											maxlength="8" 
											class="Deshabilitado"
											sentido="N" 
										/>
						<%=getIndicadorRequeridoDerecha(false)%> </inputfieldtext>
				</td>
				<td width="11%"><inputfieldtext> <span id="TFnomclie_ER" class='<%=utils.escribeEstiloLabel("REG_VENTANA.NOMCLIE")%>'></span>
						<input 
											type="text" 
											name="TFnomclie" 
											id="TFnomclie"
											value='<%=utils.getValorContexto("REG_VENTANA.NOMCLIE")%>'
											requerido="false" 
											size="60" 
											maxlength="60" 
											class="Deshabilitado"
											sentido="N" 
										/>
						<%=getIndicadorRequeridoDerecha(false)%> </inputfieldtext>
				</td>
				</tr>
			</table>
			</div>
		</div>
		</div>
		<% 	
				AtaeSvDato tmpLST_VENTANA = utils.getDatoContexto("LST_VENTANA");
				AtaeSvCompositeDatoRegistroIndexado datoLST_VENTANA;
				int largoLST_VENTANA = 0;
				if(tmpLST_VENTANA != null && tmpLST_VENTANA instanceof AtaeSvCompositeDatoRegistroIndexado){
					datoLST_VENTANA = (AtaeSvCompositeDatoRegistroIndexado)tmpLST_VENTANA;
				}else{
					datoLST_VENTANA = null;
				}
				largoLST_VENTANA = datoLST_VENTANA.getLongitud();
			%>
		<script>var largo = <%=largoLST_VENTANA%>;</script>
		<% %>
		<div class="DivContenedorCaja" style="width:970px;">
		<div class="DivTituloCaja" id="Cabecera2">
			<label class="tituloCajaIzq" id="lbl_crit_busqueda">Documentos</label>
		</div>
		<div class="Contenido">
			<div class="Contenido" style='border-top:1px solid #8c82c0; height: auto;' >
			<table width="98%" class="Generica">
				<tr>
				<th width="2%" height="20px">&nbsp;</th>
				<th width="20%" height="20px"><label>Tipo de Documento</label></th>
				<th width="16%" height="20px"><label>F. Expedici&oacute;n</label></th>
				<th width="11%" height="20px"><label>F. Vencimiento</label></th>
				<th width="11%" height="20px"><label>Fecha Alta</label></th>
				<th width="11%" height="20px"><label>Estatus Digitalizaci&oacute;n</label></th>
				<th width="20%" height="20px"><label>Plazo para Digitalizar</label></th>
				</tr>
				<%
								String clase = "", CVEDOC = "", FAMDOC = "", TPODOC = "", DESDOC = "", FOLDOC = "", FECEXP = "", FECVEN = "", FECALTA = "", ESTDIG = "", FECVEND = "";
														
								if(datoLST_VENTANA != null && largoLST_VENTANA > 0){
									AtaeSvDato tmpRegLST_VENTANA;
									AtaeSvCompositeDatoRegistro regLST_VENTANA = null;
																
									for(int i=0; i<largoLST_VENTANA; i++){
										tmpRegLST_VENTANA = datoLST_VENTANA.getElemento(i);
										if(tmpRegLST_VENTANA != null && tmpRegLST_VENTANA instanceof AtaeSvCompositeDatoRegistro){
											regLST_VENTANA = (AtaeSvCompositeDatoRegistro) tmpRegLST_VENTANA;														
									}
																		
									FAMDOC = obtenerElementoNoNulo(regLST_VENTANA, "FAMDOC");
									TPODOC = obtenerElementoNoNulo(regLST_VENTANA, "TPODOC");
									DESDOC = obtenerElementoNoNulo(regLST_VENTANA, "DESDOC");
									CVEDOC = obtenerElementoNoNulo(regLST_VENTANA, "CVEDOC");
																		
									FOLDOC = obtenerElementoNoNulo(regLST_VENTANA, "FOLDOC");
									FECEXP = obtenerElementoNoNulo(regLST_VENTANA, "FECEXP");
									FECVEN = obtenerElementoNoNulo(regLST_VENTANA, "FECVEN");
									FECALTA = obtenerElementoNoNulo(regLST_VENTANA, "FECALTA");
									ESTDIG = obtenerElementoNoNulo(regLST_VENTANA, "ESTDIG");
									FECVEND = obtenerElementoNoNulo(regLST_VENTANA, "FECVEND");
																		
									if(i%2 !=0){ clase="Pijama2"; }else{ clase="Pijama1"; }
							%>
				<tr class="<%=clase%>" height="28px">
				<td width="2%" height="10px"><input type="radio" name="radio" id="radio<%=i%>" onclick="seleccionRadio('<%=i%>', '<%=TPODOC%>', '<%=DESDOC%>', '<%=FECEXP%>', '<%=FECVEN%>', '<%=FECALTA%>', '<%=ESTDIG%>', '<%=FECVEND%>', '<%=CVEDOC%>');verDocumento('<%=ESTDIG%>');" />
				</td>
				<td width="20%" height="10px" align="center"><%=DESDOC%></td>
				<td width="11%" height="10px" align="center"><%=formateaFecha(FECEXP,"-","/")%></td>
				<td width="11%" height="10px" align="center"><%=formateaFecha(FECVEN,"-","/")%></td>
				<td width="11%" height="10px" align="center"><%=formateaFecha(FECALTA,"-","/")%></td>
				<td width="11%" height="10px" align="center"><%=ESTDIG%></td>
				<td width="20%" height="10px" align="center"><%=formateaFecha(FECVEND,"-","/")%></td>
				</tr>
				<%
									}
								}else{
							%>
				<tr class="Pijama2" height="28px">
				<td width="2%"><input type="radio" name="radio" id="radio01" onclick="#" disabled="disabled" />
				</td>
				<td width="20%" align="center">&nbsp;</td>
				<td width="11%" align="center"><%=formateaFecha(FECEXP,"-","/")%></td>
				<td width="11%" align="center"><%=formateaFecha(FECVEN,"-","/")%></td>
				<td width="11%" align="center"><%=formateaFecha(FECALTA,"-","/")%></td>
				<td width="11%" align="center"><%=ESTDIG%></td>
				<td width="20%" align="center"><%=formateaFecha(FECVEND,"-","/")%></td>
				</tr>
				<tr class="Pijama1" height="28px">
				<td width="2%"><input type="radio" name="radio" id="radio02" onclick="#" disabled="disabled" />
				</td>
				<td width="20%" align="center">&nbsp;</td>
				<td width="11%" align="center"><%=formateaFecha(FECEXP,"-","/")%></td>
				<td width="11%" align="center"><%=formateaFecha(FECVEN,"-","/")%></td>
				<td width="11%" align="center"><%=formateaFecha(FECALTA,"-","/")%></td>
				<td width="11%" align="center"><%=ESTDIG%></td>
				<td width="20%" align="center"><%=formateaFecha(FECVEND,"-","/")%></td>
				</tr>
				<tr class="Pijama2" height="28px">
				<td width="2%"><input type="radio" name="radio" id="radio03" onclick="#" disabled="disabled" />
				</td>
				<td width="20%" align="center">&nbsp;</td>
				<td width="11%" align="center"><%=formateaFecha(FECEXP,"-","/")%></td>
				<td width="11%" align="center"><%=formateaFecha(FECVEN,"-","/")%></td>
				<td width="11%" align="center"><%=formateaFecha(FECALTA,"-","/")%></td>
				<td width="11%" align="center"><%=ESTDIG%></td>
				<td width="11%" align="center"><%=formateaFecha(FECVEND,"-","/")%></td>
				</tr>
				<tr class="Pijama1" height="28px">
				<td width="2%"><input type="radio" name="radio" id="radio04" onclick="#" disabled="disabled" />
				</td>
				<td width="20%" align="center">&nbsp;</td>
				<td width="11%" align="center"><%=formateaFecha(FECEXP,"-","/")%></td>
				<td width="11%" align="center"><%=formateaFecha(FECVEN,"-","/")%></td>
				<td width="11%" align="center"><%=formateaFecha(FECALTA,"-","/")%></td>
				<td width="11%" align="center"><%=ESTDIG%></td>
				<td width="20%" align="center"><%=formateaFecha(FECVEND,"-","/")%></td>
				</tr>
				<tr class="Pijama2" height="28px">
				<td width="2%"><input type="radio" name="radio" id="radio05" onclick="#" disabled="disabled" />
				</td>
				<td width="20%" align="center">&nbsp;</td>
				<td width="11%" align="center"><%=formateaFecha(FECEXP,"-","/")%></td>
				<td width="11%" align="center"><%=formateaFecha(FECVEN,"-","/")%></td>
				<td width="11%" align="center"><%=formateaFecha(FECALTA,"-","/")%></td>
				<td width="11%" align="center"><%=ESTDIG%></td>
				<td width="20%" align="center"><%=formateaFecha(FECVEND,"-","/")%></td>
				</tr>
				<tr class="Pijama1" height="28px">
				<td width="2%"><input type="radio" name="radio" id="radio06" onclick="#" disabled="disabled" />
				</td>
				<td width="20%" align="center">&nbsp;</td>
				<td width="11%" align="center"><%=formateaFecha(FECEXP,"-","/")%></td>
				<td width="11%" align="center"><%=formateaFecha(FECVEN,"-","/")%></td>
				<td width="11%" align="center"><%=formateaFecha(FECALTA,"-","/")%></td>
				<td width="11%" align="center"><%=ESTDIG%></td>
				<td width="20%" align="center"><%=formateaFecha(FECVEND,"-","/")%></td>
				</tr>
				<tr class="Pijama2" height="28px">
				<td width="2%"><input type="radio" name="radio" id="radio07" onclick="#" disabled="disabled" />
				</td>
				<td width="20%" align="center">&nbsp;</td>
				<td width="11%" align="center"><%=formateaFecha(FECEXP,"-","/")%></td>
				<td width="11%" align="center"><%=formateaFecha(FECVEN,"-","/")%></td>
				<td width="11%" align="center"><%=formateaFecha(FECALTA,"-","/")%></td>
				<td width="11%" align="center"><%=ESTDIG%></td>
				<td width="20%" align="center"><%=formateaFecha(FECVEND,"-","/")%></td>
				</tr>
				<tr class="Pijama1" height="28px">
				<td width="2%"><input type="radio" name="radio" id="radio08" onclick="#" disabled="disabled" />
				</td>
				<td width="20%" align="center">&nbsp;</td>
				<td width="11%" align="center"><%=formateaFecha(FECEXP,"-","/")%></td>
				<td width="11%" align="center"><%=formateaFecha(FECVEN,"-","/")%></td>
				<td width="11%" align="center"><%=formateaFecha(FECALTA,"-","/")%></td>
				<td width="11%" align="center"><%=ESTDIG%></td>
				<td width="20%" align="center"><%=formateaFecha(FECVEND,"-","/")%></td>
				</tr>
				<tr class="Pijama2" height="28px">
				<td width="2%"><input type="radio" name="radio" id="radio09" onclick="#" disabled="disabled" />
				</td>
				<td width="20%" align="center">&nbsp;</td>
				<td width="11%" align="center"><%=formateaFecha(FECEXP,"-","/")%></td>
				<td width="11%" align="center"><%=formateaFecha(FECVEN,"-","/")%></td>
				<td width="11%" align="center"><%=formateaFecha(FECALTA,"-","/")%></td>
				<td width="11%" align="center"><%=ESTDIG%></td>
				<td width="20%" align="center"><%=formateaFecha(FECVEND,"-","/")%></td>
				</tr>
				<tr class="Pijama1" height="28px">
				<td width="15%"><input type="radio" name="radio" id="radio10" onclick="#" disabled="disabled" />
				</td>
				<td width="20%" align="center">&nbsp;</td>
				<td width="11%" align="center"><%=formateaFecha(FECEXP,"-","/")%></td>
				<td width="11%" align="center"><%=formateaFecha(FECVEN,"-","/")%></td>
				<td width="11%" align="center"><%=formateaFecha(FECALTA,"-","/")%></td>
				<td width="11%" align="center"><%=ESTDIG%></td>
				<td width="20%" align="center"><%=formateaFecha(FECVEND,"-","/")%></td>
				</tr>
				<tr class="Pijama2" height="28px">
				<td width="2%"><input type="radio" name="radio" id="radio11" onclick="#" disabled="disabled" />
				</td>
				<td width="20%" align="center">&nbsp;</td>
				<td width="11%" align="center"><%=formateaFecha(FECEXP,"-","/")%></td>
				<td width="11%" align="center"><%=formateaFecha(FECVEN,"-","/")%></td>
				<td width="11%" align="center"><%=formateaFecha(FECALTA,"-","/")%></td>
				<td width="11%" align="center"><%=ESTDIG%></td>
				<td width="20%" align="center"><%=formateaFecha(FECVEND,"-","/")%></td>
				</tr>
				<tr class="Pijama1" height="28px">
				<td width="2%"><input type="radio" name="radio" id="radio12" onclick="#" disabled="disabled" />
				</td>
				<td width="20%" align="center">&nbsp;</td>
				<td width="11%" align="center"><%=formateaFecha(FECEXP,"-","/")%></td>
				<td width="11%" align="center"><%=formateaFecha(FECVEN,"-","/")%></td>
				<td width="11%" align="center"><%=formateaFecha(FECALTA,"-","/")%></td>
				<td width="11%" align="center"><%=ESTDIG%></td>
				<td width="20%" align="center"><%=formateaFecha(FECVEND,"-","/")%></td>
				</tr>
				<tr class="Pijama2" height="28px">
				<td width="2%"><input type="radio" name="radio" id="radio13" onclick="#" disabled="disabled" />
				</td>
				<td width="20%" align="center">&nbsp;</td>
				<td width="11%" align="center"><%=formateaFecha(FECEXP,"-","/")%></td>
				<td width="11%" align="center"><%=formateaFecha(FECVEN,"-","/")%></td>
				<td width="11%" align="center"><%=formateaFecha(FECALTA,"-","/")%></td>
				<td width="11%" align="center"><%=ESTDIG%></td>
				<td width="11%" align="center"><%=formateaFecha(FECVEND,"-","/")%></td>
				</tr>
				<tr class="Pijama1" height="28px">
				<td width="2%"><input type="radio" name="radio" id="radio14" onclick="#" disabled="disabled" />
				</td>
				<td width="20%" align="center">&nbsp;</td>
				<td width="11%" align="center"><%=formateaFecha(FECEXP,"-","/")%></td>
				<td width="11%" align="center"><%=formateaFecha(FECVEN,"-","/")%></td>
				<td width="11%" align="center"><%=formateaFecha(FECALTA,"-","/")%></td>
				<td width="11%" align="center"><%=ESTDIG%></td>
				<td width="20%" align="center"><%=formateaFecha(FECVEND,"-","/")%></td>
				</tr>
				<tr class="Pijama2" height="28px">
				<td width="2%"><input type="radio" name="radio" id="radio15" onclick="#" disabled="disabled" />
				</td>
				<td width="20%" align="center">&nbsp;</td>
				<td width="11%" align="center"><%=formateaFecha(FECEXP,"-","/")%></td>
				<td width="11%" align="center"><%=formateaFecha(FECVEN,"-","/")%></td>
				<td width="11%" align="center"><%=formateaFecha(FECALTA,"-","/")%></td>
				<td width="11%" align="center"><%=ESTDIG%></td>
				<td width="20%" align="center"><%=formateaFecha(FECVEND,"-","/")%></td>
				</tr>
				<%	}	%>
				<%
								if(datoLST_VENTANA != null && largoLST_VENTANA > 0){
									AtaeSvDato tmpRegLST_VENTANA;
									AtaeSvCompositeDatoRegistro regLST_VENTANA = null;
															
									for(int i=0; i<largoLST_VENTANA; i++){
										tmpRegLST_VENTANA = datoLST_VENTANA.getElemento(i);
										if(tmpRegLST_VENTANA != null && tmpRegLST_VENTANA instanceof AtaeSvCompositeDatoRegistro){
											regLST_VENTANA = (AtaeSvCompositeDatoRegistro) tmpRegLST_VENTANA;														
										}
											
										FOLDOC = obtenerElementoNoNulo(regLST_VENTANA, "FOLDOC");
										FECEXP = obtenerElementoNoNulo(regLST_VENTANA, "FECEXP");
										FECVEN = obtenerElementoNoNulo(regLST_VENTANA, "FECVEN");
										FECALTA = obtenerElementoNoNulo(regLST_VENTANA, "FECALTA");
										ESTDIG = obtenerElementoNoNulo(regLST_VENTANA, "ESTDIG");
										FECVEND = obtenerElementoNoNulo(regLST_VENTANA, "FECVEND");
																	
										if(i%2 !=0){ clase="Par"; }else{ clase="Non"; }
							%>
				<%
									}
								}else{
							%>
				<%	}	%>
			</table>
			</div>
		</div>
		</div>
		<div class="DivContenedorCaja" style="width:970px;">
		<div class="DivTituloCaja" id="Cabecera2">
			<label class="tituloCajaIzq" id="lbl_crit_busqueda"></label>
		</div>
		<div class="Contenido">
			<div class="Contenido" style='border-top:1px solid #8c82c0; height: auto;' >
			<table width="98%">
				<tr>
				<td width="2%"></td>
				<td width="11%"><label style="font-color:red"><b>Tipo de Documento:</b></label></td>
				<td width="11%"><customcombo>
					<select id="TFtipodocumento" name="TFtipodocumento" estatico="false" class="CapturaNormal" onchange="validaBotonAnadir(this)">
						<%  utils.setNombreCombo("TFtipodocumento");
												utils.setComboRequerida(true);
												utils.setDescargaDinamica(false);
												utils.setCuerpoCombo(obtenerArrayString(utils.getDatoContexto("LST_COMBO")));
												utils.setSeleccionCombo(utils.getValorContexto("REG_VENTANA.CODIGO"));
												utils.setLiteralRequeridaCombo("Seleccione");
												utils.setMostrarCodigoCombo(false);
												utils.setEnviarCodigoCombo(true);
											%>
						<%=  utils.escribeCuerpoCombo(utils.getSeleccionCombo(), utils.getCuerpoCombo(), utils.isComboRequerida(), utils.getLiteralRequeridaCombo(), utils.isEnviarCodigoCombo(), utils.isMostrarCodigoCombo()) %>
					</select>
					</customcombo>
				</td>
				<td width="11%"><div id="anadir0" ><img src="<%=botones%>anadirr_0.gif" /></div>
					<div id="anadir1" style="display:none"> <img src="<%=botones%>anadirr_1.gif" onclick="activaUpload();" style="cursor: pointer;" />
						<div style="display:none;">
							<inputfile>
							<label class="cabinet">
								<input id="uploadArchivo" name="uploadArchivo" type="file" class="file" onchange="validaAnadir(this)" style="cursor: pointer;" />
							</label>
							</inputfile>
						</div>
						<input type="hidden" name="pathTransferencia" value='<%=utils.getValorContexto("RUTA_TEMPORAL")%>' />
						<input type="hidden" name="srvTransferencia" value="true" />
					</div></td>
				<td width="11%"><div id="Eliminar0"><img src="<%=botones%>eliminarr_0.gif" style="margin-left: -12px;"/></div>
					<div id="Eliminar1" style="display:none"><img style="cursor: pointer; margin-left: -12px;" name="BT_Eliminar" 	id="BT_Eliminar" 	title="Eliminar"	 src="<%=botones%>eliminarr_1.gif"	onclick="lanzaEventos('Eliminar');"/></div></td>
				</tr>
			</table>
			</div>
		</div>
		</div>
		<div class="Contenido" style="width:97%">
		<table width="100%">
			<tr>
			<td width="29%"></td>
			<td width="38%"><div id="regresar1"><img style="cursor: pointer;" name="BT_Regresar" 	id="BT_Regresar" 	title="Regresar"	 src="<%=botones%>regresar_1.gif"	onclick="lanzaEventos('regresar');" /></div></td>
			<td width="23%"><div id="verdocumento0"><img src="<%=botones%>Ver_documento_0.gif" /></div>
				<div id="verdocumento1" style="display:none"><img style="cursor: pointer;" name="BT_VerDocumento" 	id="BT_VerDocumento" 	title="Ver Documento"	 src="<%=botones%>Ver_documento_1.gif"	onclick="lanzaEventos('documentos');" /></div></td>
				<td width="10%" align="left"></td>
			</tr>
		</table>
		</div>
	</div>
	
	
  </div>
</div>
			</form>
		</customform>
	</body>
</html>