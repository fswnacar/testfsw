<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Folios Previos</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<%@ include file="/atcl_mult_mult_jsp/funciones.jsp"%>
	<SCRIPT LANGUAGE="javascript" src="/atcl_es_web_pub/js/utils.js"></SCRIPT>
	<script type="text/javascript" src="/dpmx_es_web_pub/js/jquery.js"></script>
	<script type="text/javascript" src="/dpmx_es_web_pub/js/jquery.blockUI.js"></script>
	<SCRIPT LANGUAGE="javascript" src="/dpmx_es_web_pub/js/utils_dpmx.js"></SCRIPT>
	<SCRIPT LANGUAGE="javascript" src="/dpmx_es_web_pub/js/fx_DPMXVE02300.js"></SCRIPT>
	<%@include file="/dpmx_es_web_jsp/estilosDPMX.jsp"%>
	<link rel="stylesheet" type="text/css" href="/dpmx_es_web_pub/estilos/definicionNacarLigero.css"/>
	<link rel="stylesheet" type="text/css" href="/dpmx_es_web_pub/estilos/definicionNacarLigeroChico.css"/>
	<!--Mejora Sistemas LFPDP ODT2-->
	<link rel="stylesheet" type="text/css" href="/keon_mult_mult_pub/estilos/NacarFF1024v02.css"/>
	<!------------------EECC----------------------->
	<script type="text/javascript" src="/keon_mult_mult_pub/js/versionCss.js"></script>
	<script type="text/javascript" src="/keon_mult_mult_pub/js/keonCarga.js"></script>
	<script type="text/javascript" src="/keon_mult_mult_pub/js/jquery.js"></script>
	<script type="text/javascript" src="/keon_mult_mult_pub/js/jquery.corner.js"></script>
	<script type="text/javascript" src="/keon_mult_mult_pub/js/jquery.ui.datepicker.js"></script>
	<script type="text/javascript" src="/keon_mult_mult_pub/js/jquery.blockUI.js"></script>
</head>
<body onLoad="controlSesion();inicializa();<% if (utils.getPaginaModoMensaje() != getCteServicioOK()) {%> mostrarMensajes(tipoMensajes,paramMensajes)<%}%>" bgcolor="#FFFFFF">
	<customform>
	<form name="DPMXVE02300" action="<%=utils.getDestinoFormulario()%>" method="POST" target="" ayudarapida="Formulario Nacar" onSubmit="javascript:if(!isEnviarFormulario()) return false;">
	<input type="hidden" name="evento" id="evento">
	<input type="hidden" name="flujo" id="flujo" value="<%=utils.getFlujoID()%>">
	<input type="hidden" name="ventana" id="ventana" value="<%=utils.getVentana()%>">
	<input type="hidden" name="hdDestinoFormulario" id="hdDestinoFormulario" value='<%=utils.getDestinoFormulario()%>'/>
	
	<customhidden><input name="HD_Mensaje" id="HD_Mensaje" type="hidden" value='<%=utils.getValorContexto("MENSAJE") %>' /></customhidden>
	<customhidden><input name="HD_codMensaje" id="HD_codMensaje" type="hidden" value='<%=utils.getValorContexto("CODMENSAJE") %>' /></customhidden>
	<customhidden><input id="HD_FOLIO" name="HD_FOLIO" type="hidden" value='<%= utils.getValorContexto("REG_DATOS.CD_SOLICITUD") %>' /></customhidden>
	
	<div id="pantallaGeneral" style="width:1000px; height:580px; overflow:hidden;">
		<div id="contenido" style="height:525px;">				
			<div class="DivTituloVentana" id="Cabecera" style="width:970px;">
				<label style="text-align:center" class="tituloVentanaIzq" id="tituloventana1">Folios Previos</label>
			</div>
			<ul class="Pestanas">  
				<li><a class="inactivo" href="#" onclick="lanzaEventos('Solicitud');">Solicitud</a></li>
				<li><a class="inactivo" href="#" onclick="lanzaEventos('Expediente');">Expediente</a></li>
				<li><a class="inactivo" href="#" onclick="lanzaEventos('VerReporte');">Ver Reporte</a></li>
				<li><a class="activo" href="#" >Folios Previos</a></li>
				<li><a class="inactivo" href="#" onclick="lanzaEventos('LOG');">LOG</a></li>
			</ul>
			<div class="DivContenedorCaja" style="width:970px;">
				<div class="DivTituloCaja">
					<label class="tituloCajaDer">
							<%=utils.getValorContexto("REG_DATOS.FH_ACTUAL")%>
					</label>
				</div>
				<div class="Contenido">
					<div class="Contenido" style='border-top:1px solid #8c82c0; height: auto;' >
						<table width="98%">
							<tr><td colspan="8" style="border-top:1px"></td></tr>
							<tr>
								<td width="5%"></td>
								<td width="10%"><label><b>Folio:</b></label></td>								
								<td width="15%">
									<inputfieldtext>
									<input type="text" id="txFolio" name="txFolio" value='<%= utils.getValorContexto("REG_DATOS.CD_SOLICITUD").trim() %>' size="14" requerido="false" disabled="" class="Deshabilitado" ReadOnly />
									</inputfieldtext>
								</td>
								<td width="8%"></td>
								<td width="8%"><label><b>Cliente:</b></label></td>
								<td width="10%">
									<inputfieldtext>
									<input type="text" id="txNU_Cliente" name="txNU_Cliente" value='<%= utils.getValorContexto("REG_DATOS.NU_CLIENTE").trim() %>' size="8" requerido="false" disabled="" class="Deshabilitado" ReadOnly />
									</inputfieldtext>
								</td>
								<td width="25%">
									<inputfieldtext>
									<input type="text" id="txNB_Cliente" name="txNB_Cliente" value='<%=utils.getValorContexto("REG_DATOS.NB_CLIENTE").trim() +" " +utils.getValorContexto("REG_DATOS.NB_APPATERNO").trim() +" "+ utils.getValorContexto("REG_DATOS.NB_APMATERNO").trim()%>' size="60" requerido="false" disabled="" class="Deshabilitado" ReadOnly />
									</inputfieldtext>
								</td>
								<td width="17%"></td>
							</tr>							
						</table>
					</div>
				</div>
			</div>
			<div class="DivContenedorCaja" style="width:970px;">
				<div class="DivTituloCaja"></div>
				<div class="Contenido" style="overflow:hidden; width:970px; border-top:1px solid #8c82c0; height: 300px;"id="top">
					<div id ="mainContainer" style="overflow:auto; overflow-y:scroll; width:970px; solid #8c82c0; height: 300px;" onscroll="MoverScroll('top','mainContainer',this.id);" style="overflow:auto; overflow-y:scroll; width:967px; height: 300px;">
						<table width="100%">		
							<thead>
								<tr class="CabeceraTabla">
									<th width="3%"></th>									
									<th width="12%">Folio</th>
									<th width="15%">Fecha de Solicitud</th>
									<th width="12%">Fecha L&iacute;mite de Respuesta</th>
									<th width="12%">Fecha Real de Respuesta</th>
									<th width="14%">Respuesta</th>
									<th width="19%">Motivo</th>
									<th width="13%">Estatus</th>
								</tr>
							</thead>
							<%if(utils.getValorContexto("CODMENSAJE").trim().equals("-1")){%>	
								<tbody>
									<tr class="Pijama1">
										<td class="TextoTablaCenN" colspan="8">&nbsp;</td>
									</tr>
								</tbody>						
						<%	}
							else{%>
								<tbody>
								<%	int datosLista = 0;
									String clase = "";									
									String folio = "" , fchSolicitud = "" , fchLimiteResp = "" , fchRealResp = "", respuesta = "" , motivo = "", estatus="";
									String listaProductos0 [][] = utils.obtenerArrayString(utils.getDatoContexto("REG_DATOS"));            
									datosLista = listaProductos0.length;								
									if (!utils.getValorContexto("REG_DATOS.0.CD_SOLICITUD").trim().equals("")){
										for (int i=0; i<datosLista; i++){	 	
											if (i%2 !=0){clase="Pijama2";}
											else{clase="Pijama1";}
											
											folio = (utils.getValorContexto("REG_DATOS."+i+".CD_SOLICITUD")!=null)?utils.getValorContexto("REG_DATOS."+i+".CD_SOLICITUD").trim():"";
											fchSolicitud = (utils.getValorContexto("REG_DATOS."+i+".FH_SOLICITUD")!=null)?utils.getValorContexto("REG_DATOS."+i+".FH_SOLICITUD").trim():"";
											if(fchSolicitud != ""){fchSolicitud = fchSolicitud.substring(0,10);}
											fchLimiteResp = (utils.getValorContexto("REG_DATOS."+i+".FH_LIM_RESPUESTA")!=null)?utils.getValorContexto("REG_DATOS"+i+".FH_LIM_RESPUESTA").trim():"";
											if(fchLimiteResp != ""){fchLimiteResp = fchLimiteResp.substring(0,10);}
											fchRealResp = (utils.getValorContexto("REG_DATOS."+i+".FH_RESPUESTA")!=null)?utils.getValorContexto("REG_DATOS."+i+".FH_RESPUESTA").trim():"";
											if(fchRealResp != ""){fchRealResp = fchRealResp.substring(0,10);}
											respuesta = (utils.getValorContexto("REG_DATOS."+i+".TP_RESPUESTA")!=null)?utils.getValorContexto("REG_DATOS."+i+".TP_RESPUESTA").trim():"";
											motivo = (utils.getValorContexto("REG_DATOS."+i+".NB_RESPUESTA")!=null)?utils.getValorContexto("REG_DATOS."+i+".NB_RESPUESTA").trim():"";
											estatus = (utils.getValorContexto("REG_DATOS."+i+".NB_ESTATUS")!=null)?utils.getValorContexto("REG_DATOS."+i+".NB_ESTATUS").trim():"";
									%>	
											<tr class="<%=clase%>" align="center" id="fila<%=i%>">
												<td align="center">
													<input name="RB_S" type="radio" onclick="setValores('<%=i%>','<%=folio%>');" id="RB_S<%=i%>" value="<%=i%>"/>											
												</td>
												<td align="center" id="cd_fol<%=i%>"><%=folio%> </td>
												<td align="center"><script>document.write(formateaFecha('<%=fchSolicitud%>',"-","/"))</script></td>
												<td align="center"><script>document.write(formateaFecha('<%=fchLimiteResp%>',"-","/"))</script></td>
												<td align="center"><script>document.write(formateaFecha('<%=fchRealResp%>',"-","/"))</script></td>
												<td align="center"><%=respuesta%> </td>
												<td align="center"><%=motivo%></td>
												<td align="center"><%=estatus%> </td>
											</tr>
									<%	}
									} %>
								</tbody>
							<%	}	%>
						</table>
					</div>	
				</div>
			</div>
			<div class="Contenido" style="width:97%">
				<table width="98%">
					<tr align="center">
						<td width="100%">
							<input name="btnDetalle" id="btnDetalle" type="image" style="vertical-align: bottom cursor:pointer"   border="0" onclick="lanzaEventos('detalle'); return false;"  disabled="disabled" src="/dpmx_es_web_pub/botones/detalle_1.gif" />
						</td>
					</tr>
				</table>
			</div>			
		</div>
	</div>
	</form>
	</customform>
</body>
</html>